# What's Minio ?

As explained in the [official website](https://minio.io/), Minio is a
 distributed object storage server built for cloud applications and devops.

In other words, files can be stored in an external server.

# Minio in this project

Without any change, GISSMO will use the **upload** directory to store files.

To activate Minio as file storage server for GISSMO you need to set **STORAGE\_HOST** environment variable. And probably some other one.

Check *Specific environment variable for Minio and GISSMO*.

# Specific environment variable for Minio and GISSMO

  * **STORAGE\_HOST**: Minio's URL. For example: https://minio.domain.tld:9000
  * **STORAGE\_ACCESS\_KEY\_ID**: your Minio ACCESS\_KEY\_ID (given by Minio server at start)
  * **STORAGE\_SECRET\_ACCESS\_KEY**: your Minio SECRET\_ACCESS\_KEY (given by Minio server at start)
  * **STORAGE_BUCKET_NAME**: the bucket name in which all GISSMO files will be.

# How to launch GISSMO Docker with Minio option?

You have to use environment variables with Docker **-e** option.

Imagine you have Minio server with:

  * an URL: http://gissmo-storage.domain.tld
  * an ACCESS\_KEY\_ID: AK7EXAMPLE
  * an STORAGE\_SECRET\_ACCESS\_KEY: K7MDENG/EXAMPLEKEY

```bash
docker run -it --rm -p 8002:8000 \
--link gissmo_db:db \
-e SECRET_KEY="abcdefg" \
-e STORAGE_HOST='http://gissmo-storage.domain.tld' \
-e STORAGE_ACCESS_KEY_ID="AK7EXAMPLE" \
-e STORAGE_SECRET_ACCESS_KEY="K7MDENG/EXAMPLEKEY" gissmo:1.9.2
```

**NB**: The SECRET\_KEY is used by production mode as explained in other chapters.

# Tip: Migrate from normal storage to Minio

With Docker it's possible to use the **upload** directory as it in Minio.

For an example you have */srv/www/gissmo/upload* directory with:

  * equipments
  * intervention

And Minio storage here: */srv/www/gissmo/minio*.

Just create **/srv/www/gissmo/minio/storage** and put *equipments*
 and *intervention* on it. Then launch a Minio Docker container with a volume.

Like this:

```bash
docker run -p 9000:9000 --name minio1 \
-e "MINIO_ACCESS_KEY=AK7EXAMPLE" \
-e "MINIO_SECRET_KEY=K7MDENG/EXAMPLEKEY" \
-v /srv/www/gissmo/minio/:/export/gissmo \
minio/minio server /export/gissmo
```

We consider this server is available at *gissmo-storage.domain.tld* (HTTP, port 80).

Then you can launch GISSMO Docker container like:

```bash
docker run -it --rm -p 8002:8000 \
--link gissmo_db:db \
-e SECRET_KEY="abcdefg" \
-e STORAGE_HOST='http://gissmo-storage.domain.tld' \
-e STORAGE_BUCKET_NAME='storage' \
-e STORAGE_ACCESS_KEY_ID="AK7EXAMPLE" \
-e STORAGE_SECRET_ACCESS_KEY="K7MDENG/EXAMPLEKEY" gissmo:1.9.2
```

If directories doesn't appear, check file permissions.

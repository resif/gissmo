FROM alpine:3.6

MAINTAINER Olivier DOSSMANN, olivier+dockerfile@dossmann.net

ENV GISSMO_DIR /opt/gissmo
ENV UPLOAD_ROOT /data
ENV STATIC_ROOT $GISSMO_DIR/static

COPY . $GISSMO_DIR

WORKDIR $GISSMO_DIR

RUN mkdir -p $GISSMO_DIR/static $UPLOAD_ROOT

# UPLOAD_ROOT needs permissions to upload files.
# But other files don't. Read access by uwsgi is enough.
# Except docker-entrypoint launched by root (Dockerfile USER command).
RUN apk --no-cache --update add \
        build-base \
        libpq \
        linux-headers \
        mailcap \
        postgresql-dev \
        py-configobj \
        python3 \
        python3-dev && \
    pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir --upgrade -r requirements.txt && \
    apk del \
        build-base \
        linux-headers \
        postgresql-dev \
        python3-dev && \
    rm -rf /var/cache/apk/*

VOLUME $UPLOAD_ROOT

ENTRYPOINT ["./docker-entrypoint.sh"]

EXPOSE 8000
CMD ["production"]

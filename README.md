# Gissmo

GISSMO is a free and open-source software (database and web interface) for
the management of seismic stations and related equipments.

It allows to store information and track the history of:

  * Any kind of scientific and technical equipments labelled by a serial
number
  * The sites, with their various housings, where these equipments are
installed or stored
  * The acquisition channels and the configuration of the related instruments
  * The servicing interventions on equipments and stations

Gissmo is used by seismological french network ([RESIF](http://www.resif.fr/)).

[![Build Status](https://travis-ci.org/eost/gissmo.svg)](https://travis-ci.org/eost/gissmo)
[![Documentation](https://readthedocs.org/projects/gissmo/badge/?version=latest)](http://gissmo.readthedocs.io/en/latest/)
[![Python 3.6](https://img.shields.io/badge/python-3.6-green.svg)](http://python.org/)
[![Django 1.8.18](https://img.shields.io/badge/django-1.8.18-green.svg)](http://djangoproject.com/)
[![postgreSQL 9.5](https://img.shields.io/badge/postgreSQL-9.5-green.svg)](http://postgresql.org/)
[![Minio capable](https://img.shields.io/badge/Minio-capable-blue.svg)](https://minio.io/)
[![CeCILL 2.1](https://img.shields.io/badge/License-CeCILL-blue.svg)](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html)
[![Leaflet 1.0.0](https://img.shields.io/badge/Leaflet-1.0.0-blue.svg)](http://leafletjs.com/)

# Overview

GISSMO is mostly dedicated to technical staff and managers who operate
permanent seismic networks, but it can also be used to manage other kind of
distributed geophysical networks.

External applications can query the database through a RESTFUL API,
allowing, for example, to produce StationXML dateless when combined with a
database of instrumental responses.

A GISSMO instance is used to manage all the stations of the French
broadband network (RLBP), some of the strong motion network (RAP), as
well as other regional seismic networks. Presently it  holds ~1500
equipments, ~500 sites and 3500+ interventions.

GISSMO is based on Django (python web framework), Minio (distributed
object storage server), postgreSQL database and Docker as software
container.

# Screenshots

## Site details

![Gissmo site detail appearance](./docs/img/gissmo_site_detail.jpg)

## Station map

![Gissmo station map](./docs/img/stationmap.png)

## Web API

![Gissmo web api](./docs/img/webapi.png)

# Documentation

[Gissmo documentation on Readthedocs](http://gissmo.readthedocs.io).

# License

Gissmo is licensed under the [CeCILL 2.1](./LICENSE) license.

# Contributors

* [Olivier Dossmann](https://github.com/blankoworld)
* [Fabien Engels](https://github.com/fabienengels)
* [Martin Dutil](https://github.com/mdutil)

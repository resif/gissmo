# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gissmo', '0052_intervorganism_unique_constraint'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chainconfig',
            name='value',
            field=models.ForeignKey(to='gissmo.ParameterValue', help_text='Contact administrator for any additional value(s)', verbose_name='Value'),
        ),
    ]

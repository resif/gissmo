from datetime import datetime
from django.db import migrations
from django.utils.timezone import make_aware


def change_intervention_268_date(apps, schema_editor):
    """
    Some specific old equipment have multiple intervequip with same date.
    We have to change these dates.
    Equipments that give a problem:
      - 247
      - 248
      - 249
      - 251
      - 252
      - 253
    They all comes from this intervention: 268.
    """
    new_date = datetime.strptime('1900-01-01 00:09:40', '%Y-%m-%d %H:%M:%S')
    Intervention = apps.get_model('gissmo', 'intervention')
    Intervention.objects.filter(
        pk=268).update(
            intervention_date=make_aware(new_date))


class Migration(migrations.Migration):

    dependencies = [
        ('gissmo', '0055_delete_other1_to_5'),
    ]

    operations = [
        migrations.RunPython(change_intervention_268_date)
    ]

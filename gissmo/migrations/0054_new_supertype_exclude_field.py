from django.db import migrations, models


def check_some_supertypes(apps, schema_editor):
    """
    "01. Scientifique" and "06. Ordinateur" need to be "used for acquisition".
    So we check their "used_for_acquisition" boolean field.
    """
    to_be_checked = ['01. Scientifique', '06. Ordinateur']
    EquipSupertype = apps.get_model('gissmo', 'EquipSupertype')
    for supertype in EquipSupertype.objects.all():
        if supertype.equip_supertype_name in to_be_checked:
            supertype.used_for_acquisition = True
            supertype.save()


class Migration(migrations.Migration):

    dependencies = [
        ('gissmo', '0053_add_helptext_on_channelconfig_value'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='equipsupertype',
            options={'verbose_name': 'Equipment supertype', 'ordering': ['equip_supertype_name']},
        ),
        migrations.AlterModelOptions(
            name='organism',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='equipsupertype',
            name='used_for_acquisition',
            field=models.BooleanField(default=False, verbose_name='Used in acquisition chains', help_text='Is checked this supertype will be used to filter allowed equipments on acquisition chains'),
        ),
        migrations.RunPython(check_some_supertypes),
    ]

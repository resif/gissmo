# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gissmo', '0049_auto_20160804_1533'),
    ]

    operations = [
        migrations.CreateModel(
            name='APIAllowedHost',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('ip', models.GenericIPAddressField(unique=True, verbose_name='IP Address')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'host',
                'verbose_name_plural': 'API allowed hosts',
            },
        ),
    ]

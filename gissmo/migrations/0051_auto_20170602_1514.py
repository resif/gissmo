# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import gissmo.models


class Migration(migrations.Migration):

    dependencies = [
        ('gissmo', '0050_apiallowedhost'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipdoc',
            name='document_equip',
            field=models.FileField(verbose_name='Document', upload_to=gissmo.models.equipdoc_file_name, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmodeldoc',
            name='document_equip_model',
            field=models.FileField(blank=True, upload_to=gissmo.models.equipmodeldoc_file_name, verbose_name='Document'),
        ),
        migrations.AlterField(
            model_name='intervdoc',
            name='_file',
            field=models.FileField(blank=True, upload_to='intervention/%Y/%m/%d/', verbose_name='File'),
        ),
        migrations.AlterField(
            model_name='stationdoc',
            name='document_station',
            field=models.FileField(blank=True, upload_to=gissmo.models.stationdoc_file_name, verbose_name='Document'),
        ),
    ]

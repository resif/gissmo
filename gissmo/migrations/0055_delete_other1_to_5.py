from django.db import migrations, models


def migrate_other_to_equipment(apps, schema_editor):
    """
    As we don't need more OTHER_1 to OTHER_5, we migrate existing chains to
    equipment order (instead of OTHER_1 to OTHER_5).
    OTHER_1 to OTHER_5 have number 5 to 9.
    EQUIPMENT is 4.
    """
    Chain = apps.get_model('gissmo', 'Chain')
    chains = Chain.objects.filter(order__in=[5, 6, 7, 8, 9])
    chains.update(order=4)


class Migration(migrations.Migration):

    dependencies = [
        ('gissmo', '0054_new_supertype_exclude_field'),
    ]

    operations = [
        migrations.RunPython(migrate_other_to_equipment),
        migrations.AlterField(
            model_name='chain',
            name='order',
            field=models.IntegerField(verbose_name='Type', choices=[(1, 'Sensor'), (2, 'PreAmplifier'), (3, 'DataLogger'), (4, 'Equipment')]),
        ),
    ]

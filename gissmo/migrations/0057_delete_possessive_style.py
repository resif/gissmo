# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gissmo', '0056_change_intervention_268_date'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='equipmodeldoc',
            options={'verbose_name_plural': 'Documents (equip. model)', 'verbose_name': 'Document (equip. model)'},
        ),
        migrations.AlterModelOptions(
            name='forbiddenequipmentmodel',
            options={'verbose_name': 'Forbidden Equipment model'},
        ),
        migrations.AlterModelOptions(
            name='parameterequip',
            options={'verbose_name': 'Equipment parameter'},
        ),
        migrations.AlterModelOptions(
            name='parametervalue',
            options={'verbose_name': 'Parameter value'},
        ),
    ]

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gissmo', '0051_auto_20170602_1514'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='intervorganism',
            unique_together=set([('intervention', 'organism')]),
        ),
    ]

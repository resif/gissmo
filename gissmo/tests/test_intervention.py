from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.forms import modelform_factory
from django.test import TestCase

from equipment import actions as EquipAction
from equipment import states as EquipState
from gissmo.forms import IntervEquipInlineForm
from gissmo.models import (
    EquipModel,
    EquipSupertype,
    EquipType,
    Equipment,
    IntervEquip,
    Intervention,
    Organism,
    Project,
    StationSite,
)

DEFAULT_ADMIN_LOGIN = 'admin'
DEFAULT_ADMIN_PASSWORD = 'admin'


class InterventionTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Users
        cls.superuser = User.objects.create_superuser(
            DEFAULT_ADMIN_LOGIN,
            'admin@mysite.com',
            DEFAULT_ADMIN_PASSWORD)
        cls.user1 = User.objects.create_user(
            'john',
            'john@mysite.com',
            'doe')
        # Organism
        cls.default_operator = Organism.objects.create(
            name='New OBS',
            _type=Organism.OBSERVATORY)
        # Project
        cls.default_project = Project.objects.create(
            name='MonticuleArray',
            manager=cls.user1)
        # Equipment
        cls.supertype_1 = EquipSupertype.objects.create(
            equip_supertype_name='Scientific',
            presentation_rank='1')
        cls.eq_type1 = EquipType.objects.create(
            equip_supertype=cls.supertype_1,
            equip_type_name='Velocimeter',
            presentation_rank=0)
        cls.model1 = EquipModel.objects.create(
            equip_type=cls.eq_type1,
            equip_model_name='R2-D2')

    def create_station(self, number):
        name = 'Station %s for tests' % number
        code = 'S%s' % number
        date = datetime.strptime('2017-01-20', '%Y-%m-%d')
        return StationSite.objects.create(
            site_type=StationSite.STATION,
            station_code=code,
            site_name=name,
            operator=self.default_operator,
            project=self.default_project,
            creation_date=date,
            actor=self.user1)

    def create_equipment(self, number, station):
        date = datetime.strptime('2017-02-21', '%Y-%m-%d')
        return Equipment.objects.create(
            equip_model=self.model1,
            serial_number=number,
            owner=self.default_operator,
            stockage_site=station,
            purchase_date=date,
            actor=self.user1)

    def create_intervequipform_data(self, action_id):
        """
        Create data for an IntervEquip Form.
        """
        action_name = EquipAction.EQUIP_ACTIONS[action_id - 1][1]

        # first create a station
        station = self.create_station(action_id)
        # then an equipment
        equipment = self.create_equipment(action_id, station)
        # finally create a intervention with same station
        intervention_date = datetime.strptime('2017-03-01', '%Y-%m-%d') \
            + timedelta(days=int(action_id))
        interv = Intervention.objects.create(
            station=station,
            intervention_date=intervention_date,
            note="Test specific behaviour with action '%s'" % action_name)
        return {
            'intervention': interv.id,
            'equip_action': action_id,
            'equip': equipment.id,
            'equip_state': EquipState.A_TESTER,
            'station': station.id,
        }

    def get_intervequip_form(self):
        """
        If you need specific construction of IntervEquipForm, it's here.
        """
        return modelform_factory(
            IntervEquip,
            form=IntervEquipInlineForm)

    def test_action_buy_with_same_station(self):
        """
        Buy action with same station is FORBIDDEN!
        """
        action = EquipAction.ACHETER
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertFalse(
            form.is_valid(),
            "Action %s is forbidden on same station!" % action_name)

    def test_action_test_with_same_action(self):
        """
        Test action should be POSSIBLE with same station.
        """
        action = EquipAction.TESTER
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_install_with_same_action(self):
        """
        Install action should be POSSIBLE with same station.
        """
        action = EquipAction.INSTALLER
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_uninstall_with_same_station(self):
        """
        Uninstall action with same station is FORBIDDEN!
        """
        action = EquipAction.DESINSTALLER
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertFalse(
            form.is_valid(),
            "Action %s is forbidden on same station!" % action_name)

    def test_action_report_problem_with_same_action(self):
        """
        Report problem action should be POSSIBLE with same station.
        """
        action = EquipAction.CONSTATER_DEFAUT
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_remote_preventive_with_same_action(self):
        """
        Perform remote preventive maintenance action should be
        POSSIBLE with same station.
        """
        action = EquipAction.MAINT_PREV_DISTANTE
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_remote_corrective_with_same_action(self):
        """
        Perform remote corrective maintenance action should be
        POSSIBLE with same station.
        """
        action = EquipAction.MAINT_CORR_DISTANTE
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_local_preventive_with_same_action(self):
        """
        Perform preventive maintenance locally action should be
        POSSIBLE with same station.
        """
        action = EquipAction.MAINT_PREV_SITE
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_local_corrective_with_same_action(self):
        """
        Perform corrective maintenance locally action should be
        POSSIBLE with same station.
        """
        action = EquipAction.MAINT_CORR_SITE
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_send_with_same_station(self):
        """
        Send action with same station is FORBIDDEN!
        """
        action = EquipAction.EXPEDIER
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertFalse(
            form.is_valid(),
            "Action %s is forbidden on same station!" % action_name)

    def test_action_receive_with_same_action(self):
        """
        Receive action should be POSSIBLE with same station.
        """
        action = EquipAction.RECEVOIR
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_unusable_with_same_action(self):
        """
        Report unusable action should be POSSIBLE with same station.
        """
        action = EquipAction.METTRE_HORS_USAGE
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_report_lost_equipment_with_same_action(self):
        """
        Report lost equipment action should be POSSIBLE with same station.
        """
        action = EquipAction.CONSTATER_DISPARITION
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_recover_lost_equipment_with_same_action(self):
        """
        Recover lost equipment action should be POSSIBLE with same station.
        """
        action = EquipAction.RETROUVER
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_dump_with_same_action(self):
        """
        Dump definitively action should be POSSIBLE with same station.
        """
        action = EquipAction.METTRE_AU_REBUT
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_action_other_with_same_action(self):
        """
        Other action should be POSSIBLE with same station.
        """
        action = EquipAction.AUTRE
        action_name = EquipAction.EQUIP_ACTIONS[action - 1][1]
        data = self.create_intervequipform_data(action)
        data['note'] = "IntervEquip action %s forbidden with same station!" % (
            action_name)
        IntervEquipForm = self.get_intervequip_form()
        form = IntervEquipForm(data)

        self.assertTrue(
            form.is_valid(),
            "Action %s should be possible on same station!" % action_name)

    def test_two_intervequip_with_same_date_is_forbidden(self):
        """
        Check that it's forbidden to create 2 intervention on same date
        with the same equipment.
        """
        # Create and validate form1 (intervention, specific place and date).
        data1 = self.create_intervequipform_data(EquipAction.ACHETER)
        data1['note'] = "Test same equipment cannot have twice intervention on\
same date."
        station1 = self.create_station(41)
        data1['station'] = station1.id
        IntervEquipForm = self.get_intervequip_form()
        form1 = IntervEquipForm(data1)
        self.assertTrue(form1.is_valid(), "Failed to validate form: %s" % (
            form1.errors))
        form1.save()

        # For form2 we need:
        # - same equipment
        # - another station
        # - same intervention date
        equip_id = data1.get('equip')
        interv_id = data1.get('intervention')
        station2 = self.create_station(42)
        intervention_date = Intervention.objects.get(
            pk=interv_id).intervention_date

        # create second intervention
        new_interv = Intervention.objects.create(
            station=station2,
            intervention_date=intervention_date,
            note="Test same intervention date on another station but different\
 equipment."
        )

        # Create the second form (same date and equipment but different
        # station)
        data2 = {
            'intervention': new_interv.id,
            'equip_action': EquipAction.TESTER,
            'equip': equip_id,
            'equip_state': EquipState.A_TESTER,
            'station': station2.id,
        }
        form2 = IntervEquipForm(data2)

        # Form validation should fail!
        self.assertFalse(form2.is_valid(), "It should not be possible to \
validate an intervention with an equipment that already have an intervention \
with same date.")

from datetime import datetime
from django.db.utils import IntegrityError
from django.test import TestCase

from django.contrib.auth.models import User

from gissmo.models import (
    Intervention,
    IntervOrganism,
    Organism,
    Project,
    StationSite)


DEFAULT_ADMIN_LOGIN = 'admin'
DEFAULT_ADMIN_PASSWORD = 'admin'


class Bug99Test(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Users
        cls.superuser = User.objects.create_superuser(
            DEFAULT_ADMIN_LOGIN,
            'admin@mysite.com',
            DEFAULT_ADMIN_PASSWORD)
        cls.user1 = User.objects.create_user(
            'john',
            'john@mysite.com',
            'doe')
        cls.user2 = User.objects.create_user(
            'alphonse',
            'al@mysite.com',
            'martin')

        # Organisms
        cls.organism1 = Organism.objects.create(
            name='A1')
        cls.organism2 = Organism.objects.create(
            name='A2')

        cls.organism1.users.add(cls.user1)
        cls.organism1.users.add(cls.user2)
        cls.organism2.users.add(cls.user1)

        # Projects
        cls.project1 = Project.objects.create(
            name='GLUGLU',
            manager=cls.user2)

        # Station
        cls.station = StationSite.objects.create(
            site_type=StationSite.STATION,
            station_code='ZORG',
            operator=cls.organism1,
            project=cls.project1,
            actor=cls.user1,
            creation_date='2017-06-08')

        # Intervention
        cls.interv = Intervention.objects.create(
            station=cls.station,
            intervention_date=datetime.now())

    def test_check_you_cannot_add_twice_same_organism(self):
        """
        When you have twice same organism, it generates an error (Bug #99).
        Check we cannot add twice same organism.
        """
        IntervOrganism.objects.create(
            intervention=self.interv,
            organism=self.organism1)
        try:
            IntervOrganism.objects.create(
                intervention=self.interv,
                organism=self.organism1)
        except IntegrityError:
            pass  # What we expect the test do
        else:
            self.fail(
                '%s organism linked twice on intervention!' % self.organism1)

from django.contrib.admin.widgets import AdminURLFieldWidget
from django.forms import FileInput
from django.utils.safestring import mark_safe


class AdminDownloadFileWidget(FileInput):
    """
    For file to download from admin interface.
    """
    def render(self, name, value, attrs=None):
        """
        Only display filename.
        Open link in a new tab (target="_blank").
        """
        output = []
        if value and getattr(value, "url", None):
            filename = value.name.split('/')[-1]
            output.append(
                '<a target="_blank" href="%s">%s</a> <br />' % (
                    value.url,
                    filename,
                )
            )
        output.append(super(AdminDownloadFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class URLFieldWidget(AdminURLFieldWidget):
    def render(self, name, value, attrs=None):
        widget = super(URLFieldWidget, self).render(name, value, attrs)
        return mark_safe(u'%s&nbsp;&nbsp;<input type="button" '
                         u'value="View Link" onclick="window.'
                         u'open(document.getElementById(\'%s\')'
                         u'.value)" />' % (widget, attrs['id']))

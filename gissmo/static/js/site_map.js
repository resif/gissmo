// initialization
var project_names = [];
var projects = [];
var state_names = [];
var states = [];
var type_names = [];
var types = [];
var allMarkers = [];
var currentStationDisplayed = false;
var filters = [];
var knownFilterTypes = ['project', 'type', 'state'];

var getIcon = function getIcon(t) {
    return t === 'Theoretical site' ? 'flag' :
        t === 'Potential site' ? 'stats-bars' :
        t === 'Measuring site' ? 'ios-pulse-strong' :
            'help';
};

var getMarkerColor = function getMarkerColor(s) {
    return s === 'Closed' ? 'blue' :
        s === 'In installation' ? 'white' :
        s === 'In test' ? 'purple' :
        s === 'Major failure' ? 'red' :
        s === 'Ongoing issue' ? 'black' :
        s === 'Running' ? 'green' :
        s === 'Other' ? 'orange' :
            'lightgray';
};

var getMarkerHexaColor = function getMarkerHexaColor(s) {
    return s === 'Closed' ? '#38aadd' :
        s === 'In installation' ? '#fbfbfb' :
        s === 'In test' ? '#d252b9' :
        s === 'Major failure' ? '#d63e2a' :
        s === 'Ongoing issue' ? '#303030' :
        s === 'Running' ? '#72b026' :
        s === 'Other' ? '#f69730' :
            '#a3a3a3';
};

var getIconColor = function getIconColor(s) {
    return s === 'In installation' ? 'black' :
                'white';
};

var Criteria = function(name) {
    this.name = name;
    this.stations = [];
};

Criteria.prototype = {
    addStation: function (station) {
        this.stations.push(station);
    }
};

var findCriteria = function findCriteria(criteria) {
    return criteria.name === this.toString();
};

var compareCriteria = function compareCriteria(a, b) {
    if (a.name > b.name)
      return 1;
    if (a.name < b.name)
      return -1;
    // a should equal b
    return 0;
};

var putMarkerToCriteriaTable = function putMarkerToCriteriaTable(criterium, names, marker, value) {
    var criteria = '';
    if (names.indexOf(value) === -1) {
        names.push(value);
        criteria = new Criteria(value);
        criterium.push(criteria);
    } else {
        criteria = criterium.find(findCriteria, value);
    }
    criteria.addStation(marker);
};

var Filter = function(name, type) {
    this.name = name;
    this.type = type;
};

// apply filters on map and display matching markers
var updateMarkers = function updateMarkers(map) {
    var markers = allMarkers;
    var projectFilters = 0;
    var states = [];
    var types = [];
    // first remove all markers from the map
    allMarkers.forEach(function (m) {
        map.removeLayer(m);
    });

    if (filters.length > 0) {
        filters.forEach(function (filter) {
            switch (filter.type) {
                case 'project':
                    if (projectFilters === 0) {
                        markers = [];
                    }
                    var project = searchProject(filter.name);
                    if (project !== false) {
                        project.stations.forEach(function (marker) {
                            markers.push(marker);
                        });
                        projectFilters += 1;
                    }
                    break;
                case 'state':
                    states.push(filter.name);
                    break;
                case 'type':
                    types.push(filter.name);
                    break;
                default:
                    console.log('Wrong filter type: ' + filter.type);
            }
        });
        // if no filters on states, keep all states as valid criteria
        if (states.length === 0) {
            states = state_names;
        }
        // if no filters on types, keep all states as valid criteria
        if (types.length === 0) {
            types = type_names;
        }

        // markers was build regarding available projects and selected projets.
        // If no projects selected, markers contains all markers.
        markers.forEach(function (marker) {
            if ((states.indexOf(marker.options.state) !== -1) && (types.indexOf(marker.options.type) !== -1)) {
                marker.addTo(map);
            }
        });
    }
};

function map_init_basic (map, options) {
    // Default TILES (baseLayer)
    var baseLayers = [
        {
            active: true,
            name: "Simple",
            layer: L.tileLayer(
                'http://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
                {
                    'attribution': 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="htt    p://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                    'subdomains': 'abcd',
                    'minZoom': 0,
                    'maxZoom': 18,
                    'ext': 'png'
                }
            )
        },
        {
            name: "Detailed",
            layer: L.tileLayer(
                'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                {
                    'attribution': 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
                }
            )
        },
    ];
    var overLayers = [];

    // fetch stations data
    stations.forEach(function (station) {
        // create a marker for each station
        var stationMarker = createStationMarker(map, station);
        allMarkers.push(stationMarker);
    });

    // for each project, make a LayerGroup
    var projectLayers = [];
    projects.sort(compareCriteria).forEach(function (project) {
        if (project.stations.length > 0) {
            var projectLayerGroup = L.layerGroup(project.stations);
            projectLayerGroup.type = 'project';
            projectLayers.push({
                name: project.name,
                layer: projectLayerGroup
            });
        }
    });

    // write project layers in a panel group
    if (projectLayers.length > 0) {
        overLayers.push({
            group: "Projects",
            layers: projectLayers
        });
    }

    // Create state layer groups
    var stateLayers = [];
    states.sort(compareCriteria).forEach(function (state) {
        if (state.stations.length > 0) {
            var stateLayerGroup = L.layerGroup(state.stations);
            stateLayerGroup.type = 'state';
            stateLayers.push({
                name: state.name,
                layer: stateLayerGroup
            });
        }
    });

    // write state layers in a panel group
    if (stateLayers.length > 0) {
        overLayers.push({
            group: "States",
            collapsed: true,
            layers: stateLayers
        });
    }

    // Create type layer groups
    var typeLayers = [];
    types.sort(compareCriteria).forEach(function (type) {
        if (type.stations.length > 0) {
            var typeLayerGroup = L.layerGroup(type.stations);
            typeLayerGroup.type = 'type';
            typeLayers.push({
                name: type.name,
                layer: typeLayerGroup
            });
        }
    });

    // write type layers in a panel group
    if (typeLayers.length > 0) {
        overLayers.push({
            group: "Types",
            collapsed: true,
            layers: typeLayers
        });
    }

    // display scale control
    var scale = L.control.scale({
      position: 'bottomleft',
      imperial: false,
      metric: true}
    )
    scale.addTo(map);

    // display legend
    var legend = L.control({position: 'bottomleft'});
    legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info legend');

        // loop through states and generate a label with a colored square
        state_names.sort().forEach(function (state) {
            div.innerHTML +=
                '<i style="background-color:' + getMarkerHexaColor(state) + '"></i> ' +
                state + '<br>';
        });
        // add current station legend
        if (currentStationDisplayed === true) {
            div.innerHTML += '<i style="background-color: #5b396b;"></i>Current station<br>';
        }

        div.innerHTML += '<br>';
        // add icons for site types
        type_names.sort().forEach(function (type) {
            div.innerHTML +=
                '<i class="ion-' +  getIcon(type) + '"></i>' + type + '<br>';
        });

        return div;
    };

    legend.addTo(map);

    // display control panel
    var panelLayers = new L.control.panelLayers(baseLayers, overLayers, {collapsibleGroups: true});
    map.addControl(panelLayers);
    // listener to control panel checkboxes
    map.on('overlayadd', onOverlayAdd);
    map.on('overlayremove', onOverlayRemove);

    // add fullscreen button
    map.addControl(new L.Control.Fullscreen());

    // Create a "filter reset" button
    var resetButton = document.createElement('button');
    resetButton.type = 'reset';
    resetButton.textContent = 'Reset filters';
    resetButton.addEventListener("click", function (e) {
        onClickResetButton(map);
    });
    // add reset button to the panel
    var panel = document.querySelector('.leaflet-panel-layers-overlays');
    panel.insertAdjacentElement("afterBegin", resetButton);
}

var onOverlayAdd = function onOverLayAdd(event) {
    // Add a new filter to known ones
    layerType = event.layer.type || '';
    if (knownFilterTypes.indexOf(event.layer.type) !== -1) {
        filter = new Filter(event.name, layerType);
        filters.push(filter);
        updateMarkers(this);
    }
};

var onOverlayRemove = function onOverlayRemove(event) {
    var map = this;
    removedFilterType = event.layer.type || '';
    if (knownFilterTypes.indexOf(removedFilterType) !== -1) {
        newFilters = [];
        removedFilterName = event.name;
        filters.forEach(function (filter) {
            if ((filter.name !== removedFilterName) || (filter.type !== removedFilterType)) {
                newFilters.push(filter);
            }
        filters = newFilters;
        });
        updateMarkers(map);
    }
};

// uncheck all control layer selectors and update map
var onClickResetButton = function onClickResetButton(map) {
    // reset filter list
    filters = [];
    var selectors = [];
    selectors = document.getElementsByClassName('leaflet-control-layers-selector');
    for (var i = 0; i < selectors.length; i++) {
        var selector = selectors[i];
        if ((selector.type === 'checkbox') && (selector.checked === true)) {
            selector.click();
        }
    }
    updateMarkers(map);
};

var searchProject = function searchProject(name) {
    var res = false;
    projects.forEach(function(p) {
        if (name.toString() === p.name.toString()) {
            res = p;
        }
    });
    return res;
};

// read station data and add a marker on map
var createStationMarker = function createStationMarker(map, station) {
    var operator = '';
    var code = '';
    var state = '';
    var isCurrentStation = false;
    var lastInterventionDate = '';
    if (station.operator !== '') {
        operator = station.operator;
    }
    if (station.code !== '') {
        code = station.code;
    }
    if ((station.state !== '') && (station.state !== 'None')) {
        state = station.state;
    }
    if (parseInt(station.id) === parseInt(stationId)) {
        isCurrentStation = true;
        currentStationDisplayed = true;
    }
    if (station.lastInterventionDate !== 'Unknown') {
        lastInterventionDate = station.lastInterventionDate;
    }

    // Create a marker icon related to given station:
    // - representation regarding the site type
    // - color regarding site state
    var iconName = getIcon(station.type);
    var markerColor = getMarkerColor(state);
    var iconColor = getIconColor(state);
    var coloredMarker = L.AwesomeMarkers.icon({
      icon: iconName,
      prefix: 'ion',
      markerColor: markerColor,
      iconColor: iconColor,
    });

    var marker = L.marker(
        [station.latitude, station.longitude],
        {
            code: code,
            state: state,
            type: station.type,
            operator: operator,
            url: stationUrl + station.id,
            icon: coloredMarker,
            lastInterventionDate: lastInterventionDate
        }
    );

    // Display selected station in darkpurple color
    if (isCurrentStation === true) {
        // marker.options.bounceOnAdd = true;
        // marker.options.bounceOnAddOptions = {duration: 3000, height: 150};
        marker.options.icon.options.markerColor = "darkpurple";
        marker.options.zIndexOffset = 10;
        marker.addTo(map);
    }

    // put marker in its right state group
    putMarkerToCriteriaTable(states, state_names, marker, state);
    // put marker in its right type group
    putMarkerToCriteriaTable(types, type_names, marker, station.type);
    // put marker in its right project's groups
    station.projects.forEach(function (projectName) {
        putMarkerToCriteriaTable(projects, project_names, marker, projectName);
    });

    // display code and state when add mouse over this marker
    marker.on('mouseover', onMouseOverStation);
    // close popup when mouse is out this marker
    marker.on('mouseout', onMouseOutStation);
    // go to station details on click
    marker.on('click', onClickStation);

    return marker;
};

// display popup when mouse is over station marker
var onMouseOverStation = function onMouseOverStation(e) {
    var popupContent = "Code: " + this.options.code;
    if (this.options.operator !== '') {
        popupContent += '\n<br>Operator: ' + this.options.operator;
    }
    if (this.options.state !== '') {
        popupContent += '\n<br>State: ' + this.options.state;
        if (this.options.lastInterventionDate !== '') {
            popupContent += ' (' + this.options.lastInterventionDate + ')';
        }
    }
    this.bindPopup(popupContent).openPopup();
};

// close popup when mouse is out station marker
var onMouseOutStation = function onMouseOutStation(e) {
    this.closePopup();
};

// go to station details when clicking on a station marker
var onClickStation = function onClickStation(e) {
  window.open(this.options.url);
};

// Resize map to the current screen
var resizeMap = function resizeMap() {
    screenHeight = window.innerHeight;
    headerSize = document.querySelector('#header').clientHeight;
    navigationSize = document.querySelector('.breadcrumbs').clientHeight;
    contentDiv = document.querySelector('.colM');
    contentPaddingSize = getComputedStyle(contentDiv, null).paddingTop;
    res = screenHeight - headerSize - navigationSize - parseInt(contentPaddingSize);
    document.getElementById('map').style.height = res + 'px';
    // As map view display a gray box between the legend and tiles, I refresh
    // the map with invalidateSize.
    map.invalidateSize(true);
};

window.onresize = resizeMap;
window.onload = resizeMap;

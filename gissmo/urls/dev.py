from __future__ import unicode_literals
from gissmo.urls.common import *  # NOQA

from gissmo.settings import dev

from django.conf.urls import (
    url,
    include)

if dev.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^error/500/$', 'django.views.defaults.server_error'),
        url(r'^error/404/$', 'django.views.defaults.page_not_found')
    ]

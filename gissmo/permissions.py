# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import permissions

from gissmo.models import APIAllowedHost


class WhitelistedOrIsAuthenticated(permissions.IsAuthenticated):
    """
    Allow access only to whitelisted IP or authenticated users.
    """
    def has_permission(self, request, view):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip_addr = x_forwarded_for.split(',')[0]
        else:
            ip_addr = request.META.get('REMOTE_ADDR')
        whitelisted = APIAllowedHost.objects.filter(ip=ip_addr).exists()
        if not whitelisted:
            return super(WhitelistedOrIsAuthenticated, self).has_permission(
                request,
                view)
        return whitelisted

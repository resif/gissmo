from __future__ import unicode_literals
from django import template
from django.conf import settings

from os import path


class AssignNode(template.Node):

    def __init__(self, name, value):
        self.name = name
        self.value = value

    def render(self, context):
        context[self.name] = self.value.resolve(context, True)
        return ''


def do_assign(parser, token):
    """
    Assign an expression to a variable in the current context.

    Syntax::
        {% assign [name] [value] %}
    Example::
        {% assign list entry.get_related %}
    """
    bits = token.contents.split()
    if len(bits) != 3:
        raise template.TemplateSyntaxError(
            "'%s' tag takes two arguments" % bits[0])
    value = parser.compile_filter(bits[2])
    return AssignNode(bits[1], value)


register = template.Library()
register.tag('assign', do_assign)


# Git version
@register.assignment_tag
def git_version(**kwargs):
    """
    Check .git directory exists.
    If exists: use current git version and display it.
    """
    res = ''
    filepath = path.join(settings.BASE_DIR, '.git/refs/heads/master')
    if path.exists(filepath):
        with open(filepath, 'r') as gitversion:
            res = gitversion.read().strip()

    return res

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# DESCRIPTION
# ===========
#
# delete_intervequip_duplicates.py script works on intervequip lines.
# First it parses all interventions.
# For each intervention, it parses each intervequip line and checks that
# no duplicate exists.
# If same line exists: keep ID of line
# Then at the end of script: delete all duplicates.
#
# USE
# ===
#
# Using docker, something like this should work:
#
# docker run -it --rm --link gissmo_db:db \
#     -v /path/to/script.py:/opt/gissmo/script.py gissmo:1.8 \
#     python3 ./script.py
#

# settings.py
import django
from django.conf import settings
import os

# Settings
settings.configure(
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.getenv('POSTGRES_DB', 'postgres'),
            'USER': os.getenv('POSTGRES_USER', 'postgres'),
            'PASSWORD': os.getenv('POSTGRES_PASS', 'postgres'),
            'HOST': os.getenv('POSTGRES_HOST', '127.0.0.1'),
            'PORT': os.getenv('POSTGRES_PORT', '5432'),
        }
    },
    INSTALLED_APPS=('gissmo',)
)


from gissmo.models import *  # NOQA


def check_intervention(intervention):
    """
    Return a list of duplicates for this intervention.
    """
    res = []
    for intervequip in intervention.intervequip_set.all():
        current_id = intervequip.id
        # do nothing if line is already in res
        if current_id in res:
            continue
        # search duplicates
        found = IntervEquip.objects.filter(
            intervention_id=intervention.id,
            equip_id=intervequip.equip_id,
            equip_action=intervequip.equip_action,
            equip_state=intervequip.equip_state).exclude(
                id=current_id)
        if not found:
            continue
        for line in found:
            line_id = line.id
            print("INTERVEQUIP %s found: %s" % (current_id, line_id))
            res.append(line.id)
    return res


def main():
    """
    Parse all interventions, and for each intervention checks intervequip
    lines.
    """
    to_delete = []
    print("################# START")
    for intervention in Intervention.objects.all().order_by('id'):
        duplicates = check_intervention(intervention)
        for duplicate in duplicates:
            to_delete.append(duplicate)

    print("################# DELETION")
    if to_delete:
        IntervEquip.objects.filter(id__in=to_delete).delete()
        print("%s have been deleted." % to_delete)
    print("################# END")


if __name__ == '__main__':
    django.setup()
    main()

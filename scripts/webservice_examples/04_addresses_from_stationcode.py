#!/usr/bin/env python3

# Script that check channels with given code.

import requests
import sys

# Configuration
server = 'gissmo.unistra.fr'
port = None
if port:
    server = ':'.join([server, port])

allowed_station_protocols = ["Seed link protocol", "HTTP"]

service_url = 'https://%s/api/v1/' % (server)

channel_url = service_url + 'channels/?station='
ip_url = service_url + 'ipaddresses/?equipment='
port_url = service_url + 'services/?equipment='
station_url = service_url + 'sites/?code='
network_url = service_url + 'networks/'


# Functions
def fetch_data(url):
    # Fetch URL
    request = requests.get(url)
    if request.status_code != 200:
        request.raise_for_status()
    # Use JSON result
    js = request.json()
    return js


def get_ips(equipment_id):
    res = []
    if equipment_id:
        url = ip_url + str(equipment_id)
        res = fetch_data(url)
    return res


def get_ports(equipment_id):
    res = []
    if equipment_id:
        url = port_url + str(equipment_id)
        res = fetch_data(url)
    return res


def display_channel_details(channel_details, service, ips):
    for ip in ips:
        # ip details
        address = ip.get('ip', '')
        netmask = ip.get('netmask', '')
        ip_details = '%s:%s' % (address, netmask)
        # protocol/port details
        protocol = service.get('protocol', '')
        port = service.get('port', '')
        service_details = '%s:%s' % (protocol, port)
        print('%s:%s:%s' % (channel_details, ip_details, service_details))


def display_channel_equipments(channel):
    # Info
    location = channel.get('location_code', '')
    code = channel.get('code', '')
    network_url = channel.get('network', None)
    network = ''
    if network_url:
        network = fetch_data(network_url).get('code', None)
    str_channel = '%s:%s:%s' % (network, code, location)

    equipments = channel.get('equipments', '')
    if not equipments:
        print('%s, No equipments' % str_channel)
        return
    nb_services = 0
    for equipment_url in equipments:
        equipment = fetch_data(equipment_url)
        equip_id = equipment.get('id', None)
        ips = get_ips(equip_id)
        services = get_ports(equip_id)
        for service in services:
            if service.get('protocol', None) in allowed_station_protocols:
                nb_services += 1  # Keep number of reported services
                display_channel_details(str_channel, service, ips)

    if nb_services == 0:
        print('%s, No services reported' % str_channel)


def list_channels(data):
    for channel in data:
        # Equipment details
        display_channel_equipments(channel)


def main(args=None):
    # Fetch first argument
    if not args or len(args) < 2:
        print("Need station CODE as argument")
        return 1
    station_code = args[1]

    # Check station exists
    if not fetch_data(station_url + station_code):
        print('Station %s not found!' % station_code)
        return 1

    # Get channels
    url = '%s%s' % (channel_url, station_code)
    data = fetch_data(url)
    list_channels(data)

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

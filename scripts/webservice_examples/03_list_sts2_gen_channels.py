#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Display FR | XX channels that contains forbidden equipments named:
  - STS-2 Gen 1
  - STS-2 Gen 2
  - STS-2 Gen 3
:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""

import requests

# Base url
server = 'https://gissmo.unistra.fr/api/v1/'
# server = 'http://domino.u-strasbg.fr:8002/api/v1/'

# Total
total = 0


def fetch_data(url):
    request = requests.get(url)
    if request.status_code != 200:
        request.raise_for_status()
    return request.json()

# Get FR and XX network
req_network_fr = fetch_data('%snetworks/?code=FR' % server)
req_network_xx = fetch_data('%snetworks/?code=XX' % server)

network_fr = network_xx = None
if len(req_network_fr) == 1:
    network_fr = req_network_fr[0]
if len(req_network_xx) == 1:
    network_xx = req_network_xx[0]
if not network_fr:
    raise('FR network not found!')
if not network_xx:
    raise('XX network not found!')
allowed_networks = [
    '%snetworks/%s/' % (server, network_fr['id']),
    '%snetworks/%s/' % (server, network_xx['id']),
]

# Search forbidden equipments (STS-2 Gen X)
forbidden_equipments = []
forbidden_names = [
    'STS-2 Gen 1',
    'STS-2 Gen 2',
    'STS-2 Gen 3',
]
for velocimeter in fetch_data('%sequipments/?type=Velocimeter' % (server)):
    if velocimeter['name'] in forbidden_names:
        equipment_link = '%sequipments/%s/' % (server, velocimeter['id'])
        forbidden_equipments.append(equipment_link)

print('##########################')
print('ALLOWED NETWORKS: %s' % allowed_networks)
print('FORBIDDEN EQUIPMENTS: %s' % forbidden_equipments)
print('##########################')

# Get all channels from api
channels = fetch_data(server + 'channels')

for channel in channels:
    if channel['network'] in allowed_networks:
        contains_forbidden_equipment = False
        for equipment in channel['equipments']:
            if equipment in forbidden_equipments:
                contains_forbidden_equipment = True
        # Count how many channels are targeted by forbidden models.
        # And display info about this channel
        if contains_forbidden_equipment:
            total += 1
            print('N: %s, S: %s, %s %s' % (
                channel['network'],
                channel['station'],
                channel['location_code'],
                channel['code']))

print('TOTAL: %s' % total)

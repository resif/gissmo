#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# DESCRIPTION
# ===========
#
# migrate_sts2.py script works on equipments which model is:
# - STS-2 Gen 1
# - STS-2 Gen 2
# - STS-2 Gen 3
# First it search all equipments which model name starts with 'STS-2 ' (pay
# attention to whitespace).
# Then it change the model by 'STS-2' only (and keep the generation version).
# Finally it will adapt all channels from these equipments to add all STS-2
# parameters.
# For parameters it uses the default value, except for generation: it uses
# v1 for Gen 1 version, v2 for Gen 2 version, etc.

# USE
# ===
#
# Using docker, something like this should work:
#
# docker run -it --rm --link gissmo_db:db \
#     -v /path/to/script.py:/opt/gissmo/script.py gissmo:1.8 \
#     python3 ./script.py
#

# settings.py
import django
from django.conf import settings
import os

# Settings
settings.configure(
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.getenv('POSTGRES_DB', 'postgres'),
            'USER': os.getenv('POSTGRES_USER', 'postgres'),
            'PASSWORD': os.getenv('POSTGRES_PASS', 'postgres'),
            'HOST': os.getenv('POSTGRES_HOST', '127.0.0.1'),
            'PORT': os.getenv('POSTGRES_PORT', '5432'),
        }
    },
    INSTALLED_APPS=('gissmo',)
)


from gissmo.models import *  # NOQA


def check_params(parameters):
    for param in parameters:
        if not param.parametervalue_set.filter(default_value=True):
            print('ERROR: no default value for %s!' % param.parameter_name)
            return False
    return True


def create_params(chain, params, generation_value):
    generation = 'v%s' % generation_value
    for param in params:
        value = param.parametervalue_set.filter(
            default_value=True).first()
        if param.parameter_name == 'generation':
            value = param.parametervalue_set.filter(
                value=generation).first()
        if not value:
            print('ERROR: NULL value for %s (search %s)' % (
                param.parameter_name, generation))
            continue
        ChainConfig.objects.create(
            chain_id=chain.id,
            channel_id=chain.channel.id,
            parameter_id=param.id,
            value_id=value.id)


def main():
    """
    Change all equipments that have "STS-2 Gen X" as model to "STS-2" and
    adapt all channels to have STS-2 parameters.
    Set "Generation parameter" to "X" (from "STS-2 Gen X").
    Set "Sensibility" to its default value.
    """
    destination_model = EquipModel.objects.get(equip_model_name='STS-2')
    if not destination_model:
        print('STS-2 model not found!')
        return 1
    params = destination_model.parameterequip_set.all()
    if not check_params(params):
        return 1
    print("################# START")
    equipments = Equipment.objects.filter(
        equip_model__equip_model_name__contains='STS-2 ',
        equip_model__equip_type__equip_type_name='Velocimeter')
    for equipment in equipments:
        generation = equipment.equip_model.equip_model_name.split(' ')[2]
        print("Gen %s : %s" % (generation, equipment.serial_number))
        if not generation:
            print('ERROR: generation not found for %s' % (
                equipment.serial_number))
            continue
        # First change model for equipment
        equipment.equip_model = destination_model
        equipment.save()
        # Then add new parameter to channels
        chains = Chain.objects.filter(equip=equipment.id)
        for chain in chains:
            create_params(chain, params, generation)
    print("################# END")


if __name__ == '__main__':
    django.setup()
    main()
